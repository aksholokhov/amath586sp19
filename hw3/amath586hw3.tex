\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{dsfont}
\usepackage{subcaption}
\usepackage{lipsum}
\usepackage{ragged2e}
\usepackage{caption}
\usepackage{subcaption}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\usepackage{graphicx, url}
\graphicspath{figures/}

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}

% The page Header
\lhead{\hmwkAuthorName}
\chead{\hmwkClassShort}
\rhead{\hmwkTitle}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Problem \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \qed
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework 3}
\newcommand{\hmwkDueDate}{May, 2}
\newcommand{\hmwkClassShort}{AMATH 586 }
\newcommand{\hmwkClass}{\hmwkClassShort \\ Inferring Structure of Complex Systems}
\newcommand{\hmwkClassTime}{Section A}
\newcommand{\hmwkClassInstructor}{Prof. Randall J. LeVeque}
\newcommand{\hmwkAuthorName}{\textbf{Alexey Sholokhov}}
\newcommand{\hmwkAuthorEmail}{\textit{aksh@uw.edu}}
\newcommand{\hmwkAbstract}{In this assignment we work study the behavior of the linear regression solvers on a multiclass image classification data. We show that there is an optimal balance between classifiers complexity and accuracy. We use sparsity promotion properties of LASSO to rank pixels by their importance for classification.}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass\\\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate\ at 23:00}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{1in}\\
%    \justify{\textbf{Abstract: }\hmwkAbstract}
    \vspace{1in}
}


\author{\hmwkAuthorName\\ \hmwkAuthorEmail}
\date{\today}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\paragraph{Solution} }

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}
\newcommand*{\pd}[3][]{\ensuremath{\frac{\partial^{#1} #2}{\partial #3}}}
\newcommand*{\prob}[1]{\ensuremath{\mathbb{P}\left[#1\right]}}
\newcommand*{\ind}[1]{\ensuremath{\mathds{1}_{\left[#1\right]}}}
\newcommand*{\at}[2]{\Big|_{#1}^{#2}}

%
% Different math operators
\DeclareMathOperator{\dom}{Dom}
\DeclareMathOperator{\diag}{Diag}
\DeclareMathOperator{\prox}{prox}
\DeclareMathOperator*{\proj}{proj}
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}

%% \mathbb symbols
\DeclareMathOperator{\E}{\mathbb{E}}
\DeclareMathOperator{\A}{\mathbb{A}}
\DeclareMathOperator{\R}{\mathbb{R}}
\DeclareMathOperator{\X}{\mathbb{X}}
\DeclareMathOperator{\N}{\mathbb{N}}
\DeclareMathOperator{\Q}{\mathbb{Q}}

%% \mathcal symbols
\DeclareMathOperator{\RR}{\mathcal{R}}
\DeclareMathOperator{\PP}{\mathcal{P}}
\DeclareMathOperator{\NN}{\mathcal{N}}
\DeclareMathOperator{\CC}{\mathcal{C}}
\DeclareMathOperator{\FF}{\mathcal{F}}
\DeclareMathOperator{\YY}{\mathcal{Y}}



\begin{document}
\maketitle
\newpage

\begin{homeworkProblem}
    
Consider the implicit Runge-Kutta method
\begin{equation}\label{ex1}
\begin{split}
U^* &= U^n + \frac{k}{2} f\large(U^*, t_n + k/2\large),\\
U^{n+1} &= U^n + k f\large(U^*, t_n + k/2\large).
\end{split}
\end{equation} 
The first step is Backward Euler to determine an approximation to the value
at the midpoint in time and the second step is the midpoint method using
this value.

\begin{enumerate}
\item Determine the order of accuracy of this method.
\item Plot the region of absolute stability.
\item Is this method A-stable?  Is it L-stable?
\end{enumerate}

\solution

\begin{enumerate}
    \item[a)] Applying this method to a sample problem $U' = \lambda U$ we get 
    \[
        U^* = U^n + \frac{k}{2}\lambda U^* \implies U^* = \frac{U^n}{1 - \frac{k\lambda}{2}}
    \]
    \[
        U^{n+1} = U^n + k\lambda U^* = \left(1 + \frac{z}{1-\frac{z}{2}} \right)U^n = \frac{1 + \frac{z}{2}}{1 - \frac{z}{2}}U^n = R(z)U^n 
    \]
    taylorizing R we get 
    \[
        R(z) = 1 + z(1 + \frac{z}{2} + \frac{z^2}{4} + O(z^3)) = 1 + z + \frac{z^2}{2} + \frac{z^3}{4} + O(z^4) = e^z - \frac{z^3}{2} + O(z^4)
    \] 
    so the method has the second order of accuracy
    \item[b)] The stability region of this method is illustrated on the figure \ref{fig:1b}
    \begin{figure}
        \centering
        \includegraphics{figures/1b}
        \caption{\label{fig:1b} Stability region for the implicit Runge-Kutta method (problem 1c)}
    \end{figure}
    \item[c)] The method is A-stable but not L-stable. We know that because the $R(z)$ function of this method matches with the one for trapezoidal method so it shares all the stability properties of this method as well. 
\end{enumerate}

\end{homeworkProblem}

\begin{homeworkProblem}
Plot the stability region for the TR-BDF2 method (8.6).   
You can start with the code in the
notebook {\tt Stability\_Regions\_onestep.ipynb}.

By analyzing $R(z)$, show 
that the method is both A-stable and L-stable.
Hint: To show A-stability, show that $|R(z)| \leq 1$ on the imaginary axis
and explain why this is enough.
    
    \solution
    
    First we find the $R(z)$ function for TR-BDF2 method. Applying this method to a sample problem $U' = \lambda U$ we have
    
    \[
        U^* = U^n + \frac{k\lambda}{4}(U^n + U^*) \implies U^* = U^n\frac{1+\frac{z}{4}}{1 - \frac{z}{4}}
    \]
    \[
        U^{n+1} = \frac{1}{3}(4U^* - U^n + k\lambda U^{n+1}) \implies U^{n+1} = \frac{\frac{4}{3}\left(\frac{1+\frac{z}{4}}{1 - \frac{z}{4}}\right) - \frac{1}{3}}{1 - \frac{z}{3}}U^n = \frac{5z + 12}{(z-4)(z-3)}U^n = R(z)U^n
    \]
    The stability region of this method is depicted on the figure \ref{fig:2}. In order to show $A$-stability, we show that $|R(z)|$ is upper-bounded by 1 on the left half-plane. Precisely, let $z = -a + bi$, $a > 0$, $b \in \R$, so 
    \[
        R(z) = \Bigg|\frac{5(-a+ib)+12}{(-a+ib - 4)(-a + ib - 3)} \Bigg| = \sqrt{\frac{(12-5a)^2 + 25b^2}{(a^2+7a-b^2+12)^2 + (2ab+7b)^2}} < 1
    \]
    So the method is A-stable. It also L-stable as the nominator is of order $|z|$ and the denominator is of order $|z|^2$ as $|z| \to \infty$.
    \begin{figure}
        \centering
        \includegraphics{figures/2}
        \caption{\label{fig:2} The stability region for TR-BDF2 method}        
    \end{figure}
\end{homeworkProblem}

\begin{homeworkProblem}
Let $g(x)=0$ represent a system of $s$ nonlinear equations in $s$ unknowns,
so $x\in\R^s$ and $g: \R^s \to \R^s$.  A vector $\bar
x\in\R^s$ is a {\em fixed point} of $g(x)$ if 
\begin{equation}\label{a}
\bar x = g(\bar x).
\end{equation}
One way to attempt to compute $\bar x$ is with {\em fixed point iteration}:
from some starting guess $x^0$, compute
\begin{equation}\label{b}
x^{j+1} = g(x^j)
\end{equation}
for $j=0,~1,~\ldots$.

\begin{enumerate}
\item Show that if there exists a norm $\|\cdot\|$ such that $g(x)$ is
Lipschitz continuous with constant $L<1$ in a neighborhood of $\bar x$, then
fixed point iteration converges from any starting value in this
neighborhood.
{\bf Hint:} Subtract equation (a) from (b).

\item Suppose $g(x)$ is differentiable and let $g'(x)$ be the $s\times s$
Jacobian matrix.  Show that if the condition of part (a) holds then
$\rho(g'(\bar x)) < 1$, where $\rho(A)$ denotes the spectral radius of a
matrix.

\item Consider a predictor-corrector method (see Section 5.9.4) consisting
of forward Euler as the predictor and backward Euler as the corrector, and
suppose we make $N$ correction iterations, i.e., we set
\begin{tabbing}
xxxxxxxxx\=xxxx\=\kill\\
\>$\hat U^0 = U^n + kf(U^n)$\\
\>for $j = 0,~1,~\ldots,~N-1$\\
\>\>$\hat U^{j+1} = U^n + kf(\hat U^j)$\\
\>\>end\\
\>$U^{n+1} = \hat U^N$.
\end{tabbing}
Note that this can be interpreted as a fixed point iteration for solving the
nonlinear equation
\[
U^{n+1} = U^n + kf(U^{n+1})
\]
of the backward Euler method.  Since the backward Euler method is implicit
and has a stability region that includes the entire left half plane, as
shown in Figure 7.1(b), one might hope that this predictor-corrector method
also has a large stability region.

Plot the stability region $S_N$ of this method for $N=2,~5,~10,~20,~50$ 
and observe that in fact the stability region does not grow much in size.

\item Using the result of part (b), show that the fixed point iteration
being used in the predictor-corrector method of part (c) can only be
expected to converge if $|k\lambda| < 1$ for all eigenvalues $\lambda$ of
the
Jacobian matrix $f'(u)$.  

\item Based on the result of part (d) and the shape of the stability region
of Backward Euler, what do you expect the stability region $S_N$ of part (c)
to converge to as $N\to\infty$?

\end{enumerate}

\solution
\begin{enumerate}
    \item The Lipschitz property for $g$ means that for any $x$, $y \in \R^s$ and some norm $\|.\|$ the following holds
        \[
            \|g(x) - g(y)\| \leq L\|x - y\|
        \]
        In addition, we're given that $L < 1$. 
        By the definition of $x^{j+1}$ we have
        \[
            \|x^{j+1} - \bar{x}\| = \|g(x^j) - g(\bar{x}\| \leq L\|x^j - \bar{x}\|
        \]
        applying this step recursively we get 
        \[
            \|x^{j+1} - \bar{x}\| \leq L^j\|x^0 - \bar{x}\|
        \]
        So we have convergence from any point $x^0$ which is in the region where the Lipschitz property holds.
    \item Using Taylor approximation at $\bar{x}$ we have 
        \[
            g(x) - g(\bar{x}) = g(\bar{x}) + g'(\bar{x})^T(x-\bar{x}) + o(\|x-\bar{x}\|) - g(\bar{x})
        \] 
        Using the Lipschitz property we have 
        \[
            \|g'(\bar{x})^T(x-\bar{x}) + o(\|x - \bar{x}\|) \| \leq L\|x - \bar{x}\|
        \]
        For any natural matrix norm it hold that 
        \[
            \rho(A) \leq \|A\|
        \]
        
        using the triangle inequality and dividing both sides by $\|x - \bar{x}\|$ we see that
        \[
            x \to \bar{x} \implies \rho(g'(\bar{x})) \leq \|g'(\bar{x})\| \leq L < 1
        \]
    \item We first establish the $R(z)$ function for the aforementioned method applying it to a sample problem $U' = \lambda U$:
        \[
            \hat{U^0} = U^n + k\lambda U^n = (1+z)U^n
        \]
        \[
            \hat{U^1} = U^n + k\lambda(1+z)U^n = (1 + z + z^2)U^n
        \]
        \[
            \hat{U^2} = U^n + z(1+z(1+z))U^n = (1 + z + z^2 + z^3)U^n
        \]
        Noticing the pattern we conclude that $R(z)$ for this method takes the form 
        \[
            U^{n+1} = R(z)U^n = P^N(z)U^n
        \]
        where 
        \[
            P^N(z) = 1 + z + z^2 + \dots + z^{N+1}
        \]
        Now we can plot stability regions $S_N$ for different N (see figure 
        
        \begin{figure}
            \centering
            \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figures/3c1}
            \end{subfigure}%
            \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figures/3c2}
            \end{subfigure}
            \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figures/3c3}
            \end{subfigure}%
            \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figures/3c4}
            \end{subfigure}
            \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figures/3c5}
            \end{subfigure}
            \caption{Stability regions for a predictor-corrector method with different number of correction steps N}
        \end{figure}

    \item Treating correction steps as iterations of a fixed point method for the subproblem we have
        \[
            \hat{U}^{j+1} = U^n + kf(\hat{U^j}) := g(\hat{U^j})
        \]
        The Jacobean matrices for $g$ is
        \[
            g'(x) = kf'(x)
        \]
        so using the spectral radius bound we proved in (2) we have 
        \[
            \rho(g'(x)) = \rho(kf'(x)) < 1 \implies k\lambda_{max}(f'(x)) < 1 \implies k\lambda < 1
        \]
        for all eigenvalues $\lambda$.
    \item Based on the previous result we expect the stability regions $S_N$ to converge to an intersection of the stability region of the Backward Euler method with the region $\{|z| = |k\lambda| < 1\}$ as $N \to \infty$.
\end{enumerate}

\end{homeworkProblem}

\textbf{The problem 4 is in the notebook attached to the assignment.}

\end{document}
